#!/bin/bash
##Copie de la source mkw vers ktu et sous dossiers
##Vers dépôt Cyrille.en.vrac
cp mkw_kikongo_dic_LO/mkw_CG.dic ../../cyrille-en-vrac/bionic/kituba-dic/hunspell-ktu-0.3_all/usr/share/hunspell/ktu_CD.dic
cp mkw_kikongo_dic_LO/mkw_CG.* ../../cyrille-en-vrac/bionic/kituba-dic/hunspell-mkw-0.3/usr/share/hunspell/
##Vers l'extension CG libreoffice
cp mkw_kikongo_dic_LO/mkw_CG.dic ktu_kikongo_dic_LO/ktu-CD.dic
##Vers l'extension CG Firefox
cp mkw_kikongo_dic_LO/mkw_CG.dic ktu_cd_dic_firefox/dictionaries/ktu-CD.dic
cp mkw_kikongo_dic_LO/mkw_CG.dic ../../github/dictionaries/ktu_CD/ktu-CD.dic

##Copie des aff
cp mkw_kikongo_dic_LO/mkw_CG.aff ../../cyrille-en-vrac/bionic/kituba-dic/hunspell-ktu-0.3/usr/share/hunspell/ktu_CD.aff
cp mkw_kikongo_dic_LO/mkw_CG.aff ktu_kikongo_dic_LO/ktu-CD.aff
cp mkw_kikongo_dic_LO/mkw_CG.aff ktu_cd_dic_firefox/dictionaries/ktu-CD.aff
cp mkw_kikongo_dic_LO/mkw_CG.dic ../../github/dictionaries/ktu_CD/ktu-CD.aff
##Création des archives zip pour les extensions
cd ktu_kikongo_dic_LO/
zip -r ../ktu_kikongo_dic *
cd ../
cd mkw_kikongo_dic_LO/
zip -r ../mkw_kikongo_dic *
cd ../
cd ktu_cd_dic_firefox/
zip -r ../kikongo-CD-dic *
cd ../
##Renommage des extensions LO et ff
mv ktu_kikongo_dic.zip ktu_kikongo_dic.oxt
mv mkw_kikongo_dic.zip mkw_kikongo_dic.oxt
mv kikongo-CD-dic.zip kikongo-CD-dic.xpi
#Copie dans le système de mkw
sudo cp mkw_kikongo_dic_LO/mkw_CG.dic /usr/share/hunspell/
sudo cp mkw_kikongo_dic_LO/mkw_CG.aff /usr/share/hunspell/
sudo cp mkw_kikongo_dic_LO/mkw_CG.dic /usr/share/hunspell/ktu_CD.dic
sudo cp mkw_kikongo_dic_LO/mkw_CG.dic /usr/share/hunspell/ktu_CD.dic
dpkg-deb -b hunspell-mkw-0.3_all/


